import React from 'react';
import propTypes from 'prop-types';
import ReactDOM from 'react-dom';
import '../tips/tips.scss';

class Tipsview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
    this.tips = React.createRef();
  }
  static propTypes = {
    text: propTypes.string.isRequired,
    // onDesc: propTypes.func,
  };
  static defaultProps = {
    // onDesc: () => {},
  };
  componentDidMount() {
    this.timer = setInterval(() => { this.close(); }, 2000);
    // this.close();
  }
  componentWillUnmount() {
    clearTimeout(this.timer);
    this.timer = null;
  }
  close() {
    // const id = document.getElementById('tips-box');
    const id = this.tips.current;
    // if (this.props.onDesc) this.props.onDesc();
    // console.log(id);
    const parenode = id.parentNode;
    ReactDOM.unmountComponentAtNode(parenode);
    // id.parentNode.removeChild(id);
    // const p = this.tips.parentNode;
    // ReactDOM.unmountComponentAtNode(p);
    parenode.parentNode.removeChild(parenode);
  }

  render() {
    return (
      // <div className="pull-div">
      <div ref={this.tips} id="tips-box" className="tips-box">
        <span className="tips-span">{this.props.text}</span>
      </div>
      // </div>
    );
  }
}

export default class Tips extends Tipsview {
  static show(param) {
    if (typeof param === 'string' && param.length !== 0) {
      const div = document.createElement('div');
      div.className = 'pull-div';
      // document.body.innerHtml = div;
      document.body.appendChild(div);
      ReactDOM.render(<Tipsview text={param} />, div);
    }
  }
}
