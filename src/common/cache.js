const After = `_cache_${2.0}`;

export default class Cache { // 移除，清空
  static getitem(name) {
    return sessionStorage.getItem(name + After) === 'undefined' ?// 验证
      undefined : JSON.parse(sessionStorage.getItem(name + After));
  }
  static setitem(name, v) {
    const value = typeof v === 'string' ? v : JSON.stringify(v);
    sessionStorage.setItem(name, value);
  }
  static removeitem(val) {
    sessionStorage.removeItem(val);
  }
}
