import 'fetch-ie8';
// import { Switch } from 'react-router';
// import Cookie from './help';

function ajax({ url, data, name = 'test', type, method = 'post' }) {
  let headers;
  let formData;
  // const { sessionCode = '', customer } = JSON.parse(Cookie.getCookie('loginInfo')) || {};
  // const customerId = customer ? customer.customerId : '';
  const { sessionCode = '', customer } = JSON.parse() || {};
  const customerId = customer ? customer.customerId : '';
  switch (type) {
    case 1:
      // 设置头部 text
      headers = { Accept: 'application/json', sessionCode, customerId };
      formData = data;
      break;
    case 2:
    // 设置头部 JSON格式 做登入无需用户id 和sessionCode
      headers = name === 'login' ? {
        Accept: 'application/json',
        'Content-Type' : 'application/json;charset=UTF-8 ',
      } : {
        Accept: 'application/json',
        'Content-Type' : 'application/json;charset=UTF-8 ',
        sessionCode,
        customerId,
      };
      formData = JSON.stringify(data);
      break;
    default:
      headers = { sessionCode };
      formData = data;
      break;
  }
  return new Promise((resolve, reject) => {
    fetch(url, {
      method,
      mode: 'cors',
      headers,
      body: method === 'get' ? null : formData,
    });
    setInterval(() => { resolve('成功'); }, 1000);
  }).then((res) => {
    res.json();
  }).then((rs) => {
    console.log(rs);
    const { code, status } = rs;
    if (status === 500 || status === 502) {
      // reject(rs);
    }
    if (code === 100) {
      return;
    }
    console.log('成功');
    // resolve(rs);
  }).catch(() => {
    // resolve(err);
  });
}

// function ajax ()


function postJson() {
  const Url = 'http://183.57.47.83:1080/customer/login';
  const data = {
    mobile: 'liuf',
    pwd: 'e10adc3949ba59abbe56e057f20f883e',
    orgCode: '0001',
  };
  const type = 1;
  const method = 'post';
  const name = '';
  return ajax({ Url, data, name, type, method });
}

export default postJson;
