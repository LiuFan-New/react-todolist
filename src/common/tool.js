// import {Cookie} from './help';

export function regular(imputName, time) {
  const val = time && time.includes('长期') ? '[0-9]{4}.[0-9]{2}.[0-9]{2}-\\长期' :
    '[0-9]{4}\\.[0-9]{2}\\.[0-9]{2}-[0-9]{4}\\.[0-9]{2}\\.[0-9]{2}';
  switch (imputName) {
    case 'mobile': // 手机号验证
      return '(^[0-9]{8}$)|(^[0-9]{11}$)';
    case 'cardedExpiryDate': // 手机号验证
      return val;
    default:
      return '';
  }
}
