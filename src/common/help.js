import cookie from 'cookie';

export class Cookie {
  static setCookie(name, val, option) {
    // 设置cookie
    const v = (typeof val === 'string') ? val : JSON.stringify(val);
    document.cookie = cookie.serialize(name, v, option);
  }
  static getCookie(cookieName) {
    // 获取cookie
    const p = cookie.parse(document.cookie);
    if (cookieName in p) {
      return p[cookieName];
    }
    return null;
  }
  static getJSONCookie(cookieName) {
    // 使用JSon转换
    return Cookie.getCookie(cookieName) ? JSON.parse(Cookie.getCookie(cookieName)) : '';
  }
  static deleteCookie(cookieName) {
    // 删除cookie
    cookie.setCookie(cookieName, '', { maxAge: -1 });// 存放时间maxAge
  }
}

