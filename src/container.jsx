import { React } from 'react';
import PropTypes from 'prop-types';
import Head from './components/head/head';

export default (opts = {}) => Component =>
  class extends React.PureComponent {
  static propTypes = {
    history: PropTypes.object,
    pageId: PropTypes.number,
    pageData: PropTypes.object, // 单页面数据，取到最小步骤名称
    currentStep: PropTypes.number, // 当前大步骤
    stepNames: PropTypes.array, // 所有大步骤的名称
    stepLength: PropTypes.number, // 当前大步骤有多少小步骤
    currentPage: PropTypes.number, // 当前小步骤
  }
  static defaultProps = {
    history: {},
    pageId: 0,
    pageData: {},
    currentStep: 0,
    stepNames: {},
    stepLength: 0,
    currentPage: 0,

  };
  render() {
    const { stepLength, currentPage, stepNames, currentStep, pageData: { name, remark } } = this.props;
    const mapbranch = { // 分步信息
      branchTitle: name, // 分步标题
      branchSum: stepLength, // 分步总步数
      branchCurrent: currentPage, // 当前步数
    };
    const {
      haveNav = true, // 头部导航
      statusIndex = 0, // 当前大步骤
      branch = mapbranch, // 分步信息
      headStatus = true, // 控制head进度显示
      currentStatus = true, // 当前进度显示
      headStepsArr = [], // 自己填写时，使用该数组渲染大步骤
      isShowBack = true,
      titleName = '开户',

    } = opts;
    return (
      <div styleName="container">
        {haveNav ? <Head
          history={this.props.history}
          statusIndex={statusIndex}
          branch={branch}
          headStatus={headStatus}
          currentStatus={currentStatus}
          stepNames={stepNames}
          currentStep={currentStep}
          headStepsArr={headStepsArr}
          isShowBack={isShowBack}
          titleName={titleName}
          remark={remark}
        /> : null}
        <Component {...this.props} />
      </div>
    );
  }
  };

