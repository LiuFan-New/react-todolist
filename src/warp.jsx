import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
// import { Cookie } from './common/help';// 过滤路由
import asyncload from './asyncload';
// import Juage from './views/juage';
// import Verifyview from './views/verifyView';
// import UserInedx from './views/userInedx';
// import UserLogin from './views/userLogin';
// import UserRegister from './views/userRegister';
// import App from './app';

const Juage = asyncload(() => import('./views/juage'));
const Verifyview = asyncload(() => import('./views/verifyView'));
const UserInedx = asyncload(() => import('./views/userInedx'));
const UserLogin = asyncload(() => import('./views/userLogin'));
const UserRegister = asyncload(() => import('./views/userRegister'));
const App = asyncload(() => import('./app'));
const OpenAccount = asyncload(() => import('./views/openAccunt/openAccount'));
const OneDepositStep = asyncload(() => import('./views/oneDepositStep/oneDepositStep'));

const createHistory = require('history').createHashHistory;
// 静态路由表
const routers = {
  Juage: '/juage',
  Verifyview: '/verifyview',
  UserInedx: '/userInedx',
  UserLogin: '/userLogin',
  UserRegister: '/userRegister',
  App: '/app',
  OpenAccount: '/openAccount',
  OneDepositStep: '/oneDepositStep',
};

// 检查是否登入
// function authLogin(Component, props) {
//   const sessionCode = Cookie.getCookie('sessionCode');
//   let renderComponent;
//   if (!sessionCode) {
//     renderComponent = <Juage {...props} />;
//     // alert('重新登入');
//   } else {
//     renderComponent = <Component {...props} />;
//   }
//   return renderComponent;
// }
const history = createHistory();

const Warp = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" render={() => <Redirect to={{ pathname: routers.Juage }} />} exact />
        <Route path={routers.Juage} component={Juage} />
        <Route path={routers.App} component={App} />
        <Route path={routers.Verifyview} component={Verifyview} />
        <Route path={routers.UserInedx} component={UserInedx} />
        <Route path={routers.UserRegister} component={UserRegister} />
        <Route path={routers.UserLogin} component={UserLogin} />
        <Route path={routers.OpenAccount} component={OpenAccount} />
        <Route path={routers.OneDepositStep} component={OneDepositStep} />
        {/* <Route path="/" exact render={() => <Redirect to={{ pathname: router.Juage }} />} />
        <Route path={router.App} component={App} />
         <Route path="/views/userInedx" modes={props => authLogin(UserInedx, props)} /> */}
      </Switch>
    </Router>
  );
};
export default Warp;

