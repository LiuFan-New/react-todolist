import React from 'react';
// import logo from './logo.svg';
import './app.css';


// var list=[];// var arr=[];
let ide = 0;
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ischeck: false,
      list: [],
      arr: [],
    };
    this.inputval = React.createRef();
    this.changeAll = this.changeAll.bind(this);
  }

  handlAdd = () => {
    const val = this.inputval.current.value;
    if (!val) return;
    const data = {
      id: ide,
      text: val,
      check: false,
    };
    const { list } = this.state;
    this.setState({
      list: [...list, data],
    });
    this.inputval.current.value = '';
    ide += 1;
  }
  delList = (itme) => {
    const { list } = this.state;
    list.splice(itme, 1);
    this.setState({ list });
  };
  delArr = (itme) => {
    const { arr } = this.state;
    arr.splice(itme, 1);
    this.setState({ arr });
  };

  changList = (tolist, index) => {
    const { list, arr } = this.state;
    if (tolist === 'changeToNew') {
      arr.push(list[index]); // onelist = list.filter((i, n) => index !== n);
      this.delList(index);// twolist = arr.concat(list[index]);
      this.setState({ list, arr });
    } else {
      list.push(arr[index]);
      this.delArr(index);
      this.setState({ list, arr });
    }
  }
  changeAll(e) {
    const bool = e.target.checked;
    const { list } = this.state;
    if (bool) {
      this.setState({ ischeck: true, list: list.map(item => ({ ...item, check: true })) });
    } else {
      this.setState({ ischeck: false, list: list.map(item => ({ ...item, check: false })) });
    }
  }
  delAll = () => {
    if (this.state.ischeck) {
      this.setState({ list: [], arr: [], ischeck: false });
    }
  }

  render() {
    const { list, arr } = this.state;
    return (
      <div className="box">
        <h4>ToDolist</h4>
        <div className="content">
          <input type="text" placeholder="请输入..." ref={this.inputval} className="input" />
          <button onClick={this.handlAdd}>添加</button>
          <label>
            <input type="checkbox" checked={this.state.ischeck} onChange={(e) => { this.changeAll(e); }} />全选
            <input type="button" onClick={this.delAll} value="批量删除" />
          </label>
          <div className="underway">
            <h3>正在进行</h3>
            <ul>
              {
               list.map((item, index) => (
                 <li key={item.id}>
                   <input onChange={() => this.changList('changeToNew', index)} type="checkBox" checked={item.check} />
                   {item.text}
                   <span index={item.id} onClick={(itme) => this.delList(itme)}>×</span>
                 </li>
                 ))
               }
            </ul>
          </div>
          <div className="already">
            <h3>已经完成</h3>
            <ul>
              { arr.map((item, index) => (
                <li key={item.id}>
                  <input onChange={() => this.changList('changeToList', index)} type="checkBox" checked="checked" />
                  {item.text}
                  <span index={item.id} onClick={(itme) => this.delArr(itme)}>×</span>
                </li>))
                }
            </ul>
          </div>
        </div>
        <a href="./views/juage">Juage</a>
      </div>
    );
  }
}


export default App;
