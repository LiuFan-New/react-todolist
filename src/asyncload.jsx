import React from 'react';

export default load => (
  class ContentComponent extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        com: null,
      };
    }
    componentDidMount() {
      if (this.state.com !== null) return;
      load()
        .then(mod => this.setState({ com: mod.default || mod }))
        .catch((err) => { console.log(err); throw err; });
    }
    render() {
      const { com } = this.state;
      return (com) ? <this.state.com {...this.props} /> : 'loading';
    }
  }
);

// export default ComtentComponent;
