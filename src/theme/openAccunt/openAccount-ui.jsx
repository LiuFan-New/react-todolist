import React from 'react';
// import PropTypes from 'prop-types';
// import { withRouter } from 'react-router-dom';
import './openAccount-ui.scss';
import Tips from '../../components/tips/tips';
// import ajax from '../../common/ajax';

// @withRouter
class OpenAccountUi extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bankVal: '中国农业银行',
      moneyVal: '人民币',
    };
    this.account = React.createRef();
    this.input = React.createRef();
  }
  goBack = () => {
    window.history.go(-1);
  }
  bankValue = [
    { id: 1, title: '中国农业银行' },
    { id: 2, title: '中国储蓄银行' },
    { id: 3, title: '中国建设银行' },
    { id: 4, title: '中国银行' },
  ];
  moneyValue = [
    { id: 1, title: '人民币' },
    { id: 2, title: '港币' },
    { id: 3, title: '美元' },
    { id: 4, title: '其他' },
  ];
  bank = (e) => {
    const selctval = e.target.value;
    this.setState({ bankVal: selctval });
    // console.log(bankVal);
  };
  money = (e) => {
    const selctval = e.target.value;
    this.setState({ moneyVal: selctval });
    // console.log(moneyval);
  };
  next = () => {
    const { bankVal, moneyVal } = this.state;
    const accountval = this.account.current;
    const inputval = this.input.current;
    if (bankVal === '' && moneyVal === '') return;
    if (accountval.value === '') {
      accountval.focus();
      Tips.show('请输入账号');
      return;
    }
    if (moneyVal === '4') {
      inputval.focus();
      Tips.show('输入钱的类型');
      return;
    }
    console.log(accountval);
    // const { history } = this.props;
    // history.push('/oneDepositStep');
  };
  render() {
    return (
      <div className="openaccount">
        <div className="openac-title">
          <div className="openac-back" onClick={this.goBack} >返回</div>
          <div className="openac-verify">开户</div>
        </div>
        <p>转账至中信建投(國際)證券</p>
        <div className="openac-content">
          <ul>
            <li className="openaccount-li">
              <span className="openaccount-name">银行</span>
              <span className="openaccount-text">
                <select className="openaccount-select" onChange={(e) => { this.bank(e); }}>
                  {
                   this.bankValue.map((item) => <option key={item.id} value={item.id}>{item.title}</option>)
                  }
                </select>
              </span>
            </li>
            <li className="openaccount-li">
              <span className="openaccount-name">账号</span>
              <span>
                <input type="text" className="openaccount-input" ref={this.account} />
              </span>
            </li>
            <li className="openaccount-li">
              <span className="openaccount-name">货币种类</span>
              <span>
                <select className="openaccount-select" onChange={(e) => { this.money(e); }}>
                  {
                   this.moneyValue.map((item) => <option key={item.id} value={item.id}>{item.title}</option>)
                  }
                </select>
                <div>
                  <input type="text" className="openaccount-inputs" ref={this.input} />
                </div>
              </span>
            </li>
            <li className="openaccount-li">
              <span className="openaccount-name">手续费</span>
              <span>
                <span>银行转账可能会收取手续费。</span>
              </span>
            </li>
            <li className="openaccount-li">
              <span className="openaccount-name">转账时间</span>
              <span>
                <span>转账并非实时到账，以实际到账时间为准。</span>
              </span>
            </li>
          </ul>

        </div>
        <div className="openac-button">
          <button className="buttons" onClick={this.next} >继续开户</button>
        </div>
      </div>
    );
  }
}

// OpenAccountUi.propTypes = {
//   history: PropTypes.object.isRequired,
// };

export default OpenAccountUi;
