import React from 'react';
import 'fetch-ie8';
// import Ajax from '../../common/ajax';
import '../../theme/oneDepositStep/oneDepositStep-ui.scss';
import Tips from '../../components/tips/tips';
import PageCity from '../../common/pagecity.json';

class OneDepositStepui extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadFile: '',
      data: [],
      province: [],
      city: [],
      counties: [],
      proVal: '',
      cityVal: '',
      counVal: '',
    };
  }
  // callback() {
  //   this.setState({uploadFile: uploadFile});
  // }
  handlChange = (e) => { // 图片上传 缩略图显示 accept="image/*" 允许任何类型的图片
    // const { uploadFile } = this.state;
    const allimgtxt = '.jpg|.jpeg|.bmp|.png';
    const { files, value } = e;
    const file = files[0];
    const filename = value.substring(value.lastIndexOf('.')).toLowerCase();
    if (allimgtxt.indexOf(`${filename}|`) === -1) {
      Tips.show('不是图片类型不能上传');
      // console.log(allimgtxt); 提示 不是图片类型不能上传
      return;
    }
    if (file.size > (20 * 1024 * 1024)) {
      Tips.show('请上传图片小于20M');
      // console.log(file.size); 提示 限制图片大小
      return;
    }
    this.setState({ uploadFile: file.name });
  };
  // 页面打开就去加载省份信息
  componentDidMount() { // 先要请求数据 循环放在对应的 选择器上面
    this.ajax();
  }
  ajax = () => {
    const province = [];
    const data = [];
    const url = PageCity;
    url.map(i => {
      province.push(i.name);
      data.push(i);
    });
    // cities: data[0].city,
    // counties: data[0].city[0].area
    this.setState({ province, city: data[0].city, counties: data[0].city[0].area, data });
  }

  handlProvinces = (e) => { // 改变省份随之改变的是市
    e.preventDefault(); // 取消事件的默认行为和默认事件
    const { data } = this.state;
    // for (var i in data) {
    //   if(i === e.target.value ) {
    //     this.setState({ city: data[i].city, counties: data[i].city[0].area, province: e.target.value });
    //   }
    // }

    data.forEach((i, n) => {
      if (n.toString() === e.target.value) {
        this.setState({ city: data[n].city, counties: data[n].city[0].area, proVal: e.target.value, cityVal: '', counVal: '' });
      }
    });
  }
  citys = (e) => {
    e.preventDefault();
    const { city } = this.state;
    city.forEach((i, n) => {
      if (n.toString() === e.target.value) {
        this.setState({ counties: city[n].area, cityVal: e.target.value });
      }
    });
  }

  countie = (e) => {
    e.preventDefault();
    this.setState({ counVal: e.target.value });
  }
  render() {
    const { province, city, counties } = this.state;
    const { proVal, cityVal, counVal } = this.state;
    return (
      <div>
        <div className="pic"> <img src={this.state.uploadFile} alt="" /> </div>
        <div className="pic-content">图片上传：
          <input type="file" accept="image/*" onChange={(e) => { this.handlChange(e.target); }} />
        </div>
        {/* <Ajax /> 省市县三级联动 */}
        {
        province.length !== 0 ?
          <div>
            <select className="province" onChange={(e) => this.handlProvinces(e)} >
              {/* <option value>省份</option> */}
              {
                province.map((itme, index) => <option value={index} key={itme.id}>{itme}</option>)
              }
            </select>
            <select className="city" onChange={(e) => this.citys(e)}>
              {/* <option value="">市</option> */}
              {
                city.map((itme, index) => <option value={index} key={itme.id}>{itme.name}</option>)
              }
            </select>
            <select name="" id="" onChange={(e) => this.countie(e)}>
              {/* <option value="">区</option> */}
              {
                counties.map((itme, index) => <option value={index} key={itme.id}>{itme}</option>)
              }
            </select>
          </div>
      : <div>{proVal}+{cityVal} +{counVal}</div>
      }
        <div><h6>{proVal}+{cityVal} +{counVal}</h6></div>
      </div>
    );
  }
}

export default OneDepositStepui;

