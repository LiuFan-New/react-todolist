import React from 'react';
import PropTypes from 'prop-types';
// import Cookie from '../common/help';
import Cache from '../common/cache';
import '../views/juage.scss';
import Tips from '../components/tips/tips';

class Juage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectAreaVal: 0,
      // loginType: 1,
    };
    this.phoenval = React.createRef();
  }

  hanglnext = () => {
    const { selectAreaVal } = this.state;
    const userPhoen = this.phoenval.current.value;
    if (userPhoen === '') {
      // const tip = '手机不可为空';
      Tips.show('手机不可为空');
      return;
    }
    // 选到select的值
    // const mobilelist = this.mobilelist[Number(selectAreaVal)].value;
    if (selectAreaVal === '+86') { // 判断长度
      if (!Number(userPhoen) || userPhoen !== 11) {
        // alert('手机格式输入不正确');
        Tips.show('手机格式输入不正确');
        return;
      }
    } else {
      const regexp = '(^[0-9]{8}$)|(^[0-9]{11}$)';
      if (new RegExp(regexp).test(userPhoen)) {
        Tips.show('请输入正确的手机格式');
        return;
      }
    }
    const { history } = this.props;
    // Cache.setitem('loginType', loginType);
    Cache.setitem('userPhoen', JSON.stringify(`${selectAreaVal} ${userPhoen}`));
    // Cookie.setitem('selectmoblieVal', selectAreaVal);
    history.push('/verifyView');
  }

  mobilelist = [
    { name: '大陆', val: 0, value: '+86' },
    { name: '香港', val: 1, value: '+852' },
    { name: '澳门', val: 2, value: '+853' },
    { name: '台湾', val: 3, value: '+886' },
  ];
  render() {
    return (
      <div className="loginpage">
        <div className="loginimg"> <img src="../01.jpg" className="img" alt="" />
        </div>
        <div className="content">
          <span className="login-span" >welcome login</span>
          <select stylename="selectArea" onChange={(v) => { this.setState({ selectAreaVal: v.target.value }); }} >
            {
              this.mobilelist.map((item) => <option key={item.val} value={item.value}>{item.value}</option>)
            }
          </select>
          <input className="login-input" type="text" placeholder="请输入手机号..." ref={this.phoenval} />
        </div>
        <div className="login-botton">
          <button className="buttons" onClick={this.hanglnext} >下一步</button>
        </div>
        {/* <a href="/app">app</a> */}
      </div>
    );
  }
}
Juage.propTypes = {
  history: PropTypes.object.isRequired,
};

export default Juage;
