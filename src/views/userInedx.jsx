import React from 'react';
import PropTypes from 'prop-types';
import '../views/userInedx.scss';

class UserInedx extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      identityVal: '0号身份证',
      certificationVal: '网上',
    };
    this.idtiVal = React.createRef();
    this.certVal = React.createRef();
  }
  goBack = () => {
    window.history.go(-1);
  }
  handlChange = (e, name) => {
    const selctval = e.target.value;
    // let {identityVal, certificationVal} = this.state;
    if (name === 'identity') {
      this.setState({ identityVal: selctval });
    } else {
      this.setState({ certificationVal: selctval });
    }
  };
  identity = [
    { id: 1, title: '0号身份证' },
    { id: 2, title: '1号身份证' },
    { id: 3, title: '2号身份证' },
    { id: 4, title: '3号身份证' },
  ];
  certification = [
    { id: 1, title: '网上' },
    { id: 2, title: '现金' },
  ];
  next = () => {
    const { identityVal, certificationVal } = this.state;
    // no change no shuzhi
    if (identityVal === '' && certificationVal === '') {
      return;
    } // 拿到 select 里面的值 存起来
    const { history } = this.props;
    console.log(identityVal);
    history.push('/openAccount');
  }
  render() {
    return (
      <div className="nationalityTax">
        <header className="nationalityTax-title" >
          <div className="nationalityTax-back" onClick={this.goBack} >返回</div>
          <div className="nationalityTax-verify">开户</div>
        </header>
        <div className="nationalityTax-shengfen">
          <label>选择身份证类型</label>
          <select value="0号身份证" className="fen" onChange={(e) => this.handlChange(e, 'identity')}>
            {
              this.identity.map((item) => <option key={item.id} value={item.value} >{item.title}</option>)
            }
          </select>
        </div>

        <div className="nationalityTax-renzheng">
          <label>认证方式</label>
          <select value="网上" className="fen" onChange={(e) => this.handlChange(e, 'certifi')}>
            {
              this.certification.map((item) => <option value={item.value} key={item.id}>{item.title}</option>)
            }
          </select>
          <div className="login-botton">
            <button className="buttons" onClick={this.next} >继续开户</button>
          </div>
        </div>
        {/* <a href="/views/userRegister">userRegister</a> */}
      </div>
    );
  }
}

UserInedx.propTypes = {
  history: PropTypes.object.isRequired,
};
export default UserInedx;
