import React from 'react';
import Tips from '../components/tips/tips';

class UserLogin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputtype: 'password',
      showpass: 0,
    };
    this.inputvalue = React.createRef;
    // 再跳到这个页面之前，是会输入手机号在判断是否注册，注册之后再能进行登入
  }
  Login = () => {
    const val = this.inputvalue.current.value;
    //  从cookie判断之前存入的密码与现在登入的密码是否一致
    if (val === '123456') {
      Tips.show('成功');
    } else {
      Tips.show('密码不正确');
    }
  }
  changeshowpass = () => {
    const { showpass } = this.state;
    let changepass;
    let inputtype = '';
    if (showpass === 0) {
      changepass = 1;
      inputtype = 'text';
    } else {
      changepass = 0;
      inputtype = 'password';
    }
    this.setState({ showpass: changepass, inputtype });
  }

  render() {
    const { showpass, inputtype } = this.state;
    const stylepass = showpass === 0 ? 'show' : 'hide';
    return (
      <div className="login">
        <div className="login-top"><h4>登入</h4></div>
        <div className="login-context">
          <input type={inputtype} placeholder="输入密码" ref={this.inputvalue} />
          <span onClick={this.changeshowpass}>{stylepass}</span>
          <button onClick={this.Login}>确定</button>
        </div>
        <a href="/views/verifyView">忘记密码？</a>
      </div>
    );
  }
}
export default UserLogin;
