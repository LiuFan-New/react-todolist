import React from 'react'; // 验证码
import PropTypes from 'prop-types';
import './verifyView.scss';
import Tips from '../components/tips/tips';
// import _debounce from 'debounce';

const debounce = require('debounce');// 抖动

class Reactview extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      time: 59,
      ShowTime: false,
      timer: {},
    };
    this.inputval = React.createRef();
  }
  componentDidMount() {
    this.getCode();
  }
  setTime = () => {
    let { time, timer } = this.state;
    this.setState({ ShowTime: true }, () => { console.log(); });
    // let { time, isShowTime} = this.state;
    // console.log(ShowTime);
    timer = setInterval(() => {
      // console.log(time);
      time -= 1;
      this.setState({ time, timer }, () => {
        if (time === 0) {
          clearInterval(timer);
          this.setState({ ShowTime: false, time: 59 });
        }
      });
    }, 1000);
  }
  getCode =() => { // 页面打开就需要获取验证码
    // console.log('短信已经发送');
    Tips.show('短信已经发送');
    this.setTime();
  }
  next = () => {
    const verify = this.inputval.current;
    // console.log(verify.value);
    if (verify.length === 0 && verify === ' ') {
      Tips.show('手机验证码不可为空');
      // return;
    } else if (verify.value !== '1234') {
      Tips.show('验证码不正确');
      // return;
    } else {
      const { history } = this.props;
      history.push('/userInedx');
    }
  }
  getTime = () => {
    const { ShowTime } = this.state;
    if (!ShowTime) {
      this.getCode();
    }
  }

  goBack = () => {
    window.history.go(-1);
  }
  render() {
    const { time, ShowTime } = this.state;
    return (
      <div className="checkbox">
        <header className="title" >
          <div className="back" onClick={this.goBack} >返回</div>
          <div className="verify">验证码</div>
        </header>
        <div className="context">
          <input type="text" placeholder="填写验证码.." ref={this.inputval} maxLength="4" className="verify-input" />
          <span className="checknum" onClick={this.getTime} >{ShowTime ? `${time}s` : '重新获取'}</span>
        </div>
        <div className="botton">
          <button className="btn-next" onClick={debounce(this.next, 500)} >下一步</button>
        </div>
      </div>
    );
  }
}

Reactview.propTypes = {
  history: PropTypes.object.isRequired,
};

export default Reactview;
