import React from 'react';
// import { number } from 'prop-types';
import Tips from '../components/tips/tips';

class UserRegister extends React.PureComponent {
    state = {
      showPass1: 0,
      showPass2: 0,
      inputType1: 'password',
      inputType2: 'password',
    }
    componentWillMount() {
      this.password1 = React.createRef();
      this.password2 = React.createRef();
    }
    comfirm = () => {
      const pass1 = this.password1.current.value;
      const pass2 = this.password2.current.value;
      // console.log(number(pass1));
      if (pass1.length < 8) {
        Tips.show('长度要大于八');
        return;
      } else if (pass1 !== pass2) {
        Tips.show('两次输入不一致');
        return;
      }
      console.log(pass1);
    }
    changePassState1 = () => {
      const { showPass1 } = this.state;
      let changepass;
      let inputType1 = '';
      if (showPass1 === 0) {
        changepass = 1;
        inputType1 = 'text'; // this.setState({ showPass1: 1, inputType1: 'text' });
      } else {
        changepass = 0;
        inputType1 = 'password'; // this.setState({ showPass1: 0, inputType1: 'password' });
      }
      this.setState({ showPass1: changepass, inputType1 });
    }
    changePassState2 = () => {
      const { showPass2 } = this.state;
      let changepass;
      let inputType2 = '';
      if (showPass2 === 0) {
        changepass = 1;
        inputType2 = 'text';// this.setState({ showPass2: 1, inputType2: 'text' });
      } else {
        changepass = 0;
        inputType2 = 'password'; // this.setState({ showPass2: 0, inputType2: 'password' });
      }
      this.setState({ showPass2: changepass, inputType2 });
    }

    render() {
      const { showPass1, showPass2, inputType1, inputType2 } = this.state;
      const stylePass1 = showPass1 === 0 ? 'show' : 'hide';
      const stylePass2 = showPass2 === 0 ? 'show' : 'hide';
      return (
        <div className="Register">
          <div className="tip-title"> <h4>设置密码</h4> </div>
          <div className="one-content">
            <input type={inputType1} ref={this.password1} placeholder="输入大于8位数的密码" />
            <span onClick={this.changePassState1}>{stylePass1}</span>
          </div>
          <div className="two-content">
            <input type={inputType2} ref={this.password2} placeholder="再次输入密码" />
            <span onClick={this.changePassState2}>{stylePass2}</span>
            <button onClick={this.comfirm}>确定</button>
          </div>
        </div>
      );
    }
}
export default UserRegister;
